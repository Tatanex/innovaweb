<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Create a client with a base URI



Route::get('/', 'SiteController@index');


Route::get('contenedor/show', 'ContenedorController@index');
Route::get('contenedor/create', 'ContenedorController@create');
Route::get('contenedor/store', 'ContenedorController@store');

Route::group(['prefix'=>'contenedor'], function () {

    Route::get('mapa', ['as' => 'contenedor.mapa', 'uses' => 'ContenedorController@mapa']);
    Route::get('indice', ['as' => 'contenedor.indice', 'uses' => 'ContenedorController@indice']);
    Route::get('lista', ['as' => 'contenedor.lista', 'uses' => 'ContenedorController@lista']);
    Route::get('{id}', ['as' => 'contenedor.ver', 'uses' => 'ContenedorController@ver']);
    Route::get('{id}/detalle', ['as' => 'contenedor.detalle', 'uses' => 'ContenedorController@detalle']);


});


//Route::get('contenedor/show', function ()
//{
//    $client = new Client(['base_uri' => 'http://iothings.cl/contenedor/']);
//    $allcontenedores = $client->request('GET', 'lista');
//    echo $allcontenedores->getBody();
//});


Route::any('{all}', function(){
    return view('errors.404');
})->where('all', '.*');
