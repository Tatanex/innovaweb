<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use GuzzleHttp;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Input;
use Redirect;
use Illuminate\Http\Request;
use Psr\Http\Message\RequestInterface;
use GuzzleHttp\Psr7\Response;



class ContenedorController extends Controller
{




    /**
     * Retorna vista para mostrar listado con todos los contenedores
     *
     */
    public function indice()
    {
        return view('contenedor.indice');
    }

    /**
     * Retorna listado con todos los contenedores registrados en la api
     *
     * @return \Illuminate\Http\Response
     */
    public function lista()
    {
        $client = new Client(['base_uri' => 'http://iothings.cl/contenedor/']);
        $allcontenedores = $client->request('GET', 'lista');

        $content = $allcontenedores->getBody()->getContents();
        $jsonContenedores = json_decode($content);


        return response()->json($jsonContenedores, 200);
    }

    /**
     * Retorna vista para mostrar detalle de contenedor identificado por $id
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function ver($id)
    {
        return view('contenedor.ver');
    }

    /**
     * Retorna información detallada del contenedor identificapo por $id
     *
     * @param $id
     * @return mixed
     */
    public function detalle($id)
    {
        $client = new Client(['base_uri' => 'http://iothings.cl/contenedor/']);
        $allcontenedores = $client->request('GET', $id);

        $content = $allcontenedores->getBody()->getContents();
        $jsonContenedores = json_decode($content);

        return response()->json($jsonContenedores, 200);

    }

    /**
     * Retorna vista con mapa para todos los contenedores
     */
    public function mapa()
    {
        return view('contenedor.mapa');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("contenedor.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request1)
    {

        $id = \Illuminate\Support\Facades\Input::get('id');
        $latitud = \Illuminate\Support\Facades\Input::get('latitud');
        $longitud = \Illuminate\Support\Facades\Input::get('longitud');
        $nivel = \Illuminate\Support\Facades\Input::get('nivel');


        $client = new Client(['base_uri' => 'http://iothings.cl/']);

            $client->request('POST','contenedor', [
               'headers'  => ['Content-Type' => 'application/json'],
               'body' => json_encode(
                    [
                        'id' => $id,
                    ]),

        ]);
        $client->request('POST','contenedor/'.$id.'/posicion', [
            'headers'  => ['Content-Type' => 'application/json'],
            'id' => $id,
            'body' => json_encode(
                [
                    'latitud' => $latitud,
                    'longitud' => $longitud,
                ]),
        ]);

//        new GuzzleHttp\Psr7\Request('POST','http://iothings.cl/contenedor/{id}/posicion',$headers,$body);


//        $headers =   ['Content-Type' => 'application/json'];
//        $body  =    json_encode(['latitud' => $latitud,
//            'longitud' => $longitud]);
//        $respuesta = $client->send($request);
//        $respuesta->getStatusCode();
//        dd($respuesta);
//        $respuesta=  $request->getBody()->getContents();
        \Session::flash('message', 'Se ha creado exitosamente el contenedor!');
        return view('layout');
    }

//    public function postposicion( $id,  $latitud,  $longitud){
//
//        $idcontenedor = $id;
//        $latitudcontenedor = $latitud;
//        $longitudmcontenedor = $longitud;
//
//        $client = new Client(['base_uri' => 'http://iothings.cl/contenedor/{idcontenedor}/']);
//        $request = new $client->request('POST','posicion', [
//            'headers'  => ['Content-Type' => 'application/json'],
//            'body' => json_encode(
//                [
//                    'latitud' => $latitudcontenedor,
//                    'longitud' => $longitudmcontenedor,
//                ])
//        ]);
//
//
//    }


    public function postbasura(){



    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
