@extends('home')

@section('custom-styles')
    <link href="{{ asset('leafletjs/leaflet.css')}}" rel="stylesheet" />


@stop

@section('contenido')

    <div class="row">
        <div class="col-md-12">
            <div id="map" class="map map-home" style="margin:12px 0 12px 0;height:800px;"></div>
        </div>

    </div>




@section('custom-scripts')
    <script src="{{ asset('leafletjs/leaflet.js') }}"></script>

    <script>

        $(document).ready(function () {

            $.ajax({
                'url': "{{route('contenedor.lista')}}",
                'method': 'GET',
                success: function (datos) {
                    var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                            osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                            osm = L.tileLayer(osmUrl, {maxZoom: 18, attribution: osmAttrib});
                    var map = L.map('map').setView([-36.7533437, -73.0930833, 16.22], 17).addLayer(osm);

                    var contenedorIcon = L.icon({
                        iconUrl: "{{asset('contenedor.png')}}",
                        iconSize: [32, 32], // size of the icon
                        iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
                        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
                    });






                    var contenedores = datos.contenedores;
                    console.log(contenedores);

                    for (var i = 0; i < contenedores.length; ++i) {
                        L.marker([contenedores[i].posicion.latitud, contenedores[i].posicion.longitud], {icon: contenedorIcon})


//                        target="_blank"

                            .bindPopup('<a href="http://iothings.api/contenedor/'+contenedores[i].id +'/"  target="_blank">' + "ID:" + contenedores[i].id + '</a>')
                            //.openPopup()
                                .addTo(map);
                    }



                },
            });
        });
    </script>
@stop

@stop