@extends('home')



@section('contenido')
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">CREAR CONTENEDOR</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->



        <form class="form-horizontal" action="{{ url('contenedor/store') }}"  >
            <div class="box-body">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label for="id" class="col-sm-2 control-label">Contenedor ID</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="id" id="id" placeholder=" ejemplo id: 23455,1234,1245">
                    </div>
                </div>

                <div class="form-group">
                    <label for="latitud" class="col-sm-2 control-label">Latitud (grados decimales) </label>

                    <div class="col-sm-10">
                        <input type="number" step="any" class="form-control" name="latitud" id="latitud" placeholder="40.7127837 ">
                    </div>
                </div>


                <div class="form-group">
                    <label for="longitud" class="col-sm-2 control-label">Longitud (grados decimales)</label>

                    <div class="col-sm-10">
                        <input type="number" step="any" class="form-control" name="longitud" id="longitud" placeholder="-74.0059413 ">
                    </div>
                </div>

                <div class="form-group">
                    <label for="nivel" class="col-sm-2 control-label"> Nivel Basura </label>

                    <div class="col-sm-10">
                        <input type="number" class="form-control" name="nivel" id="nivel" placeholder=" 1 - 100% ">
                    </div>
                </div>


                <div  class="text-center col-sm-3"  style="float:left;margin-left:450px;" >
                    <button type="submit" class="btn btn-block btn-info btn-lg" value="Crear Contenedor">CREAR</button>
                </div>

            </div>
            <!-- /.box-body -->
        </form>
    </div>
@stop
