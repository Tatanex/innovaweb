@extends('home')

@section('custom-styles')

    <link href="{{ asset('vendors/datatables/css/jquery.dataTables.css')}}" rel="stylesheet"  type="text/css" />

    @stop

    @section('contenido')

        <table class="table table-striped table-bordered table-hover" id="tabla-contenedores"  cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Nivel Basura</th>
                <th>Acciones</th>
            </tr>
            </thead>
            <tfoot>
            <tr>
                <th>ID</th>
            </tr>
            </tfoot>
            <tbody>
            </tbody>
        </table>


    @section('custom-scripts')
            <!-- Dependencias para DataTable -->
        <script src="{{ asset('vendors/datatables/js/jquery.dataTables.js') }}"></script>
        <script src="{{ asset('vendors/datatables/js/dataTables.bootstrap.js') }}"></script>

        <script>

            $(document).ready(function () {

                // inicializa tabla
                var tablaContenedores = $("#tabla-contenedores").DataTable({});

                // obtiene listado de contenedores
                $.ajax({
                    'url':"{{route('contenedor.lista')}}",
                    'method':'GET',
                    success: function (datos) {
                        var contenedores = datos.contenedores;
                        var contenedor;
                        var url;
                        for (var i in contenedores) {
                            contenedor = contenedores[i];
                            url = "{{route('contenedor.ver')}}".replace("%7Bid%7D", contenedor.id);
                            var row = tablaContenedores.row.add([
                                contenedor.id,
                                contenedor.basura.nivel,
                                '<a href="' + url + '"><i class="fa fa-fw fa-eye text-primary"></i></a>'
                            ]).draw().node();

                        }
                    }
                });
            });
        </script>
    @stop

@stop