@extends('home')

@section('custom-styles')
    <link href="{{ asset('leafletjs/leaflet.css')}}" rel="stylesheet" />


@stop

@section('contenido')

   <div class="row">
       <div class="col-md-8">
           <div id="map" class="map map-home" style="margin:12px 0 12px 0;witdh:800px;height:600px;"></div>
       </div>
       <div class="col-md-4">
           <div class="box box-solid">
               <div class="box-header with-border">
                   <i class="fa fa-text-width"></i>

                   <h3 class="box-title">Detalle Contenedor</h3>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                   <dl class="dl-horizontal">
                       <dt>Nivel basura:</dt>
                       <dd id="nivel-basura"></dd>
                       <dt>Lectura basura:</dt>
                       <dd id="timestamp-basura"></dd>
                       <dt>Nivel Co2: </dt>
                       <dd id="nivel-co2"></dd>
                       <dt>Nivel 02</dt>
                       <dd id="nivel-o2"></dd>


                   </dl>
               </div>
               <!-- /.box-body -->
           </div>
           <!-- /.box -->
       </div>
   </div>




@section('custom-scripts')
    <script src="{{ asset('leafletjs/leaflet.js') }}"></script>

    <script>

        $(document).ready(function () {

            var contenedorId = "{{Route::current()->getParameter('id')}}";

            $.ajax({
                'url': "{{route('contenedor.detalle')}}".replace("%7Bid%7D", contenedorId),
                'method': 'GET',
                success: function (datos) {
                    console.log(datos);
                    var contenedor = datos.contenedor;
                    var contenedorLatitud = contenedor.posicion.latitud;
                    var contenedorLongitud = contenedor.posicion.longitud;
                    var contenedorBasura = contenedor.basura;
                    var contenedorBateria = contenedor.bateria;

                    var osmUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                            osmAttrib = '&copy; <a href="http://openstreetmap.org/copyright">OpenStreetMap</a> contributors',
                            osm = L.tileLayer(osmUrl, {maxZoom: 18, attribution: osmAttrib});
                    var map = L.map('map').setView([contenedorLatitud, contenedorLongitud, 16.22], 17).addLayer(osm);

                    var contenedorIcon = L.icon({
                        iconUrl: "{{asset('contenedor.png')}}",
                        iconSize: [32, 32], // size of the icon
                        iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
                        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
                    });

                        L.marker([contenedorLatitud, contenedorLongitud], {icon: contenedorIcon})

                                .bindPopup('<a href="" target="_blank">' + "ID:" + contenedor.id + '</a>')
                            //.openPopup()
                                .addTo(map);

                    $("#nivel-basura").html(contenedorBasura.nivel + "");
                    $("#timestamp-basura").html(contenedorBasura.timestamp);

                    $("#nivel-co2").html('<span class="label label-success">100 ppm</span>');
                    $("#nivel-o2").html('<span class="label label-success">5,5 ppm</span>');
                },
            });
        });
    </script>
@stop

@stop