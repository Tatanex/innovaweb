@extends('home')

@section('header')
    Error 404 - Página no encontrada
@stop


@section('contenido')

    <div class="panel panel-danger">
        <div class="panel-heading">
            <h3 class="panel-title text-center">Atención!</h3>
        </div>
        <div class="panel-body text-center">
            La página que Usted solicitó no se encuentra
        </div>
    </div>



@stop